# -*- coding: utf-8 -*-
import logging
import logging.config
import globVar
import datetime

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)
    exit(1)

if __name__ == "__main__":
    # date and time
    now = datetime.datetime.now()
    currentTime = now.strftime("%Y-%m-%d %H:%M:%S")
    delta = datetime.timedelta(hours=-1)
    deltaTime = (now + delta).strftime("%Y-%m-%d %H:%M:%S")
    print currentTime, deltaTime
    #
