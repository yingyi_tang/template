import logging
import logging.config
import globVar
import os
import commands

#####
# USAGE: house keeping for output, tmp dir and log file
#####
# Version
# 2019/12/06, Stable version, Yingyi Tang
#####

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("main")
except Exception, e:
    print(e)

def houseKeeping():
    # Clean output dir FILES
    cmd = "/bin/find {dir}/ -mtime +1 -type f -exec rm {char} \;".format(dir=globVar.OUTPUT_DIR, char="{}")
    status, ret = commands.getstatusoutput(cmd)
    if status == 0:
        logger.info("ran {cmd}".format(cmd=cmd))
    else:
        logger.error("ran {cmd} failed".format(cmd=cmd))
    # Clean tmp dir FILES
    cmd = "/bin/find {dir}/ -mtime +1 -type f -exec rm {char} \;".format(dir=globVar.TMP_DIR, char="{}")
    status, ret = commands.getstatusoutput(cmd)
    if status == 0:
        logger.info("ran {cmd}".format(cmd=cmd))
    else:
        logger.error("ran {cmd} failed".format(cmd=cmd))
    # Clean log file, 50M
    fsize = os.path.getsize(globVar.LOG_FILE) / 1024 / 2014
    if fsize >= 50:
        os.remove(globVar.LOG_FILE)
        logger.info("log file size is {size}M, removed {file}".format(size=fsize, file=globVar.LOG_FILE))
    else:
        pass

if __name__ == "__main__":
    houseKeeping()
