#!/usr/bin/env python
import os

#####
# USAGE: For other module to import to use
#####
# Version
# 2019/12/06, Stable version, Yingyi Tang
#####

# BIN DIR
BASE_DIR = os.path.split(os.path.realpath(__file__))[0]
# Directory
SW_DIR = os.path.abspath(os.path.join(BASE_DIR, ".."))
CONF_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "conf"))
DOCS_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "docs"))
LOG_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "log"))
INPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "input"))
OUTPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "output"))
TMP_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "tmp"))
# Files
LOG_FILE = os.path.abspath(os.path.join(LOG_DIR, "template.log"))
LOGGER_FILE = os.path.abspath(os.path.join(CONF_DIR, "logger.ini"))
SYS_FILE = os.path.abspath(os.path.join(CONF_DIR, "sys.ini"))
VERSION_FILE = os.path.abspath(os.path.join(BASE_DIR, "..", "version.json"))

if __name__ == "__main__":
    print "BASE_DIR: " + BASE_DIR
    print "SW_DIR: " + SW_DIR
    print CONF_DIR
    print DOCS_DIR
    print LOG_DIR
    print INPUT_DIR
    print OUTPUT_DIR
    print TMP_DIR
    print LOG_FILE
    print LOGGER_FILE
    print SYS_FILE
    print VERSION_FILE