import os
import json
import sys
import datetime
import globVar
import shutil

#####
# USAGE: update_version.py <version> <description> <author>
# Example: update version.py "0.0.1.181101" "first version" "Yingyi Tang"
#####
# Version
# 2019/12/06, Stable version, Yingyi Tang
#####

def backup():
    tmpDir = globVar.TMP_DIR
    versionFile = os.path.join(tmpDir, "version.json.tmp")
    shutil.copy(globVar.VERSION_FILE, versionFile)

def updateVersion():
    if len(sys.argv) < 4:
        print("Missing parameter!")
        exit(1)
    swName = globVar.SW_DIR.split(os.path.sep)[len(globVar.SW_DIR.split(os.path.sep))-1]
    now = datetime.datetime.now()
    time = now.strftime("%Y/%m/%d %H:%M:%S")
    version = sys.argv[1]
    desc = sys.argv[2]
    author = sys.argv[3]
    newVersionDic = {"description":desc, "author": author, "date": time}
    print version,newVersionDic

    versionFile = globVar.VERSION_FILE
    with open(versionFile, "r") as f:
        origVersionContent = json.load(f)
        #print origVersionContent
    if version in origVersionContent.keys():
        print "{version} version already exist".format(version=version)
    else:
        origVersionContent[version] = str(newVersionDic)
        #print origVersionContent

    with open(versionFile, "w") as j:
        json.dump(origVersionContent, j, indent=1, sort_keys=True)

if __name__ == "__main__":
    backup()
    updateVersion()
